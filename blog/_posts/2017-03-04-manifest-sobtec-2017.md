---
layout: post
title: "Manifest Congrés 2017"
date: 2017-03-04 10:00:00
image: '/assets/img/'
description: 'Manifest de presentació del II Congrés de Sobirania Tecnològica, realitzar 4 de març de 2017,
al Centre Cívic Joan Oliver "Pere Quart" - Barcelona'
tags:
- Congrés 2017
author_name: 'Sobtec'
author_info: 'grup promotor'
author_contact: 'info@sobtec.cat'
icon: 'icon-sobtec'
---

La tecnologia és tot aquell conjunt de coneixement tècnic que ens permet adaptar-nos al medi ambient. La nostra història està plena de millores tecnològiques que ens han portat a poder viure millor. Des del grup promotor Sobtec, volem centrar-nos en un seguit de tecnologies, que comencen a agafar rellevància a partir dels anys 60, i que emmarquem dins del terme de tecnologies informàtiques.

### Sobirania i Tecnologia
L'expansió i evolució de les tecnologies informàtiques han generat canvis en la nostra manera de treballar; relacionar-nos i comunicar-nos amb una velocitat que no deixa espai a la reflexió. S'ha propiciat, per una banda, el subjecte passiu que consumeix productes i serveis, i per una altra, s'ha accentuat la cosificació de les relacions socials a través de la compra d'aquests productes. És en aquest punt on és important l'empoderament col·lectiu per aconseguir que, aquestes millores tecnològiques, ens portin a satisfer les nostres necessitats i a poder viure millor en el nostre medi ambient.

### Ser sobirans tecnològicament
Ser sobirans tecnològicament comporta tenir els coneixements i la capacitat per decidir sobre la tecnologia. Quan les programadores dels anys 60 escrivien codi pels colossals ordinadors de l'època, aquell codi era un bé comú que compartien entre elles, i formava part del coneixement d'aquella comunitat. No era una mercaderia. Però als anys 80, seguint la lògica del capital, es va mercantilitzant aquell codi, es van afegir restriccions als usuaris en format de llicències, i es va acabar amb l'hàbit de compartir el coneixement. L'any 1985, amb la fundació de la Free Software Fundation, neix un moviment que trenca amb l'espiral mercantilitzador del programari, i posa les bases per garantir les llibertats perdudes, i tornar a desenvolupar per la comunitat, s'anomenà programari lliure. D'aquesta manera, s'obre la via per poder ser sobirans tecnològicament en un àmbit tan important com és el del programari.

### El moment que estem vivint
Però l'evolució de les tecnologies informàtiques està acompanyada d'una constant pèrdua de sobirania: el programari lliure no té una presència destacable en sectors on, a priori, hauria de ser hegemònic, com el de l'educació o l'administració pública; la capacitat tecnològica de captar i analitzar dades de forma massiva s'està utilitzant per mercantilitzar les dades privades de les persones; les millores tecnològiques en sistemes d'informació, debat i presa de decisions no s'apliquen per ampliar les estructures democràtiques dels estats; i aquesta nova economia extractiva, sorgida gràcies a aquesta evolució tecnològica, se salta la legalitat i intenta normalitzar els seus serveis.

### El Mobile World Congress
I és en aquesta conjuntura que ens trobem amb un Mobile World Congress que capta l'atenció de mitjans internacionals i monopolitza la informació dels mitjans locals amb tres missatges: la presentació de les novetats tecnològiques de l'any, l'enorme quantitat de diners que es mouen en aquest sector, i Barcelona com el nou hub tecnològic i empresarial del sud d'Europa.   

Però darrere d'aquesta pantalla informativa, s'amaguen dos objectius que porten a grans empreses a gastar-se milions per tenir una presència destacada en aquesta fira. El primer és promoure el fetitxe del producte tecnològic, amagant-nos les relacions socials que hi ha darrere, tant en la producció dels aparells electrònics, com en el desenvolupament del programari que els fa funcionar, com en el seu consum. El segon és lliurar una nova batalla per aconseguir ser l'empresa que controla la tecnologia: la gestió de les dades dels usuaris, la xarxa per la qual viatgen, o les transferències bancàries. Un camp de batalla on l'estat hi participa com a àrbitre a temps parcial, i on el capital li ha guanyat la partida a la sobirania.

### Sobtec 2017
En aquest context proposem trobar-nos al Sobtec. Creiem que Barcelona ha de ser, i pot ser, pionera en el desenvolupament de tecnologies enfocades en la satisfacció de necessitats col·lectives. Per això volem obrir un espai de debat i aprenentatge col·lectiu per parlar i entendre, entre d'altres, els models on la sobirania li ha guanyat la partida al capital, per veure com es pot legislar a favor de la sobirania, per explicar com es pot aplicar la tecnologia per enfortir les estructures democràtiques i resoldre necessitats compartides, o per parlar del paper de les dones en les tecnologies informàtiques. En definitiva, per trobar-nos, compartir i fer créixer la sobirania tecnològica.
